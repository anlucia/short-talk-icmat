\documentclass[10pt,compress]{beamer}
\usepackage{appendixnumberbeamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{fontspec}
\usepackage{unicode-math}
\usepackage{lualatex-math}

\usetheme{metropolis}

\usetikzlibrary{patterns, positioning, intersections, calc, arrows, fit, through, fadings, patterns}
\tikzset{onslide/.code args={<#1>#2}{ \only<#1>{\pgfkeysalso{#2}}}}
\tikzfading[name=fade out, inner color=transparent!0, outer color=transparent!100]

\newcommand{\columnsbegin}{\begin{columns}}
\newcommand{\columnsend}{\end{columns}}

\usepackage{physics}
\usepackage{dsfont}

\newcommand{\field}[1]{\ensuremath{\mathds{#1}}}
\newcommand{\ring}{\mathds}
\newcommand{\Z}{\ring{Z}}
\newcommand{\CC}{\ring{C}}

\DeclareMathOperator{\supp}{supp}
\DeclareMathOperator{\diam}{diam}
\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\Ent}{Ent}


\newcommand{\identity}{\mathds{1}}
\newcommand{\bounded}{\ensuremath{\mathcal B}}
\newcommand{\hs}{\ensuremath{\mathcal H}}
\newcommand{\lind}{\ensuremath{\mathcal L}}
\newcommand{\matrixalg}{\ensuremath{\mathcal M}}
\newcommand{\alg}{\ensuremath{\mathcal A}}
\newcommand{\dirichelet}{\ensuremath{\mathcal E}}
\newcommand{\ent}[2]{D\qty(#1\|#2)}
\newcommand{\entA}[3]{D_{#1}\qty(#2\|#3)}
\newcommand{\Expect}{\mathds{E}}

\newcommand{\mcl}{\mathcal}

% fixing appendix translate issues
\pdfstringdefDisableCommands{%
  \def\translate#1{#1}%
}

\graphicspath{{./figures/}}

\title{Quantum semigroups and mixing times}
\author[A. Lucia]{Angelo Lucia (UCM - ICMAT)
}

\usepackage[export]{adjustbox}
\titlegraphic{
  \center
  \includegraphics[height=15pt, valign=t]{FSE.png}
  \hfill
  \includegraphics[height=15pt, valign=t]{MICINN.png}
  \hfill
  \includegraphics[height=40pt, valign=c]{UCM.png}
}
\date{ICMAT October 22, 2021}
\setbeamertemplate{section in toc}[sections numbered]

\begin{document}
\maketitle

\begin{frame}[standout]
  \center How to describe the dynamics \\ of quantum systems?
\end{frame}


\begin{frame}[t]{Quantum evolutions}

  \begin{block}{Schrödinger equation}
    Time evolution is given by a unitary group $U(t) = \exp(-i t H)$, where $H\in
    \bounded(\hs)$ is an Hermitian operator known as \alert<1>{Hamiltonian}.
      \[ \dv{t} \rho(t) = -i \comm{H}{\rho(t)}, \quad \rho(t) = U_t \rho(0)
    U_t^*. \]
\end{block}

   \only<2,3>{
     \begin{block}{Evolution (open systems)}
       When the system is not isolated, it interacts with an environment: $\hs =
       \hs_{S} \otimes \hs_{E}$. The state at time $t\ge 0$ is given by
       \[
         \rho_S(t) = \Tr_E U_t [\rho_S(0) \otimes \rho_E(0)] U_t^*,
       \]
       i.e. a \textbf{completely positive trace preserving} map (a \alert<2>{quantum channel}).
     \end{block}
     }
     \only<3>{
       \alert{Problem}: we have broken the group structure! The evolution cannot be described
       by the reduced state $\rho_S(t)$.
     }
     \only<4>{
       \begin{block}{Weak-coupling limit / Born-Markov approximation}
         The evolution is given by a \alert<1>{quantum Markov semigroup}: a continuous semigroup of \alert{completely positive trace-preserving} (CPTP) maps
         \[ \dv{t} \rho(t) = L\, \rho(t), \quad \rho(t) = T_t(\rho(0)) \]
       \end{block}
       The generator $L$ is known as the \alert{Lindbladian}.
     }
\end{frame}

\begin{frame}[standout]
  \center How to describe many body quantum systems?
\end{frame}


\begin{frame}{Many-body quantum systems}

  \columnsbegin
    \begin{column}{0.75\textwidth}
  \begin{itemize}[<+->]
  \item $\Gamma$ infinite graph (for example $\Gamma = \Z^D$)
  \item a finite-dimensional Hilbert space $\hs_u$
  \item for each $\Lambda \subset \Gamma$ finite, $\hs_{\Lambda} = \bigotimes_{u\in \Lambda}
    \hs_u$
  \item $\mcl A_{\Lambda} = \mcl B(\mcl H_{\Lambda})$
  \item if $\Lambda' \subset \Lambda$ then $\mcl A_{\Lambda'} \hookrightarrow \mcl A_{\Lambda}$ via $O_{\Lambda'} \mapsto O_{\Lambda'}
    \otimes \identity_{\Lambda \backslash \Lambda'}$
    \item $\supp O$ = the minimal $\Lambda$ such that $O \in \mcl A_{\Lambda}$
    \item the generator $L$ is \alert{local}
      \[  L_\Lambda = \sum_{u \in \Lambda}   L_u ;\quad \supp  L_u = B_u(r) \]
      in the sense that $L_u: \mcl A_{B_u(r)} \to \mcl A_{B_u(r)}$
    \end{itemize}
  \end{column}
  \begin{column}{0.3\textwidth}
    \begin{center}
    \begin{tikzpicture}
      \tikzstyle{node}=[shape=circle,draw=blue!70,fill=blue!50,scale=0.5];
      \tikzstyle{smallnode}=[draw=red!60, fill=red!50, scale=0.3];
      \draw[step=.5cm] (-1.4,-1.4) grid (1.4,1.4);
      \only<2>{
        \foreach \x in {-1,-0.5,...,1}
        \foreach \y in {-1,-0.5,...,1}
        {
          \node[smallnode] at (\x,\y) {};
        }
      }
      \only<3-4>{
        \foreach \x in {-0.5,0,...,0.5}
        \foreach \y in {-0.5,0,...,0.5}
        {
          \node[smallnode] at (\x,\y) {};
        }
        \draw[dashed] (-0.75,-0.75) rectangle (0.75,0.75) node {$\Lambda$};
      }
      \node (A) at (0,0) {};
      \node (B) at (0.8,0) {};             
     \only<5-6>{
       \node [draw=blue!70, fill=blue!20, fill opacity=0.4, circle through=(B),label={[label distance=0.5
         cm]north:$\supp O$}] at (A) {};
       \node[smallnode] at (A) {};       
       }
       \only<7>{
         \node [draw=blue!70, fill=blue!20, fill opacity=0.4, circle through=(B),label={[label distance=0.5cm]north:$B_r(u)$}] at (A) {};
         \node[node,label=right:$u$] at (A) {};
       }
       \only<5-7>{
       \foreach \x in {-0.5,0.5} {
         \node[smallnode] at (0,\x) {};
         \node[smallnode] at (\x,0) {};
         \node[smallnode] at (\x,\x) {};
         \node[smallnode] at (\x,-\x) {};
       }
     }
      \end{tikzpicture}
    \end{center}
  \end{column}
  \columnsend

\end{frame}



\begin{frame}[standout]
  \center An example: thermalization.
\end{frame}

\begin{frame}{Thermalization}
  \begin{block}{Gibbs state:}
    For a (local) Hamiltonian $H_{\Lambda} = \sum_{u} H_u$, the Gibbs state at inverse temperature $\beta = 1/T$ is defined as
    \[
      \sigma_{\Lambda, \beta} = \frac{1}{Z_{\beta}} e^{-\beta H_{\Lambda}}
    \]
    It is a full rank state (i.e., $\sigma >0$).
  \end{block}
\pause
  \begin{block}{Thermalization:}
    A generator $L_\Lambda$ with $\sigma_{\Lambda, \beta}$ as the \emph{unique} steady state, and \alert{ergodic}:
    \[ T_t(\rho) \rightarrow \sigma_{\Lambda, \beta} \quad t\to \infty \]
    for any initial state $\rho$.
  \end{block}

  There are a few different constructions for \emph{commuting} Hamiltonians.
  \[ \comm{H_u}{H_v} = 0,\quad \forall u,v\in \Lambda \]
\end{frame}

\begin{frame}[standout]
  \center How to quantify the speed of thermalization?
\end{frame}


\begin{frame}{Mixing time}
  If $\{T_t\}_{t\ge0}$ has a unique fixed point $\sigma$, then
  $T_t(\rho) \to \sigma$ for all $\rho$.
  \begin{block}{Mixing time}
    The \emph{mixing time} is given by
    \[ \tau_{\text{mix}}(\epsilon) = \inf \Big\{t \ge 0 \mid \sup_{\substack{\rho \ge 0 \\ \tr \rho =1}}
      \norm{T_t(\rho) -\sigma}_1 \le \epsilon \Big\} \]
  \end{block}
\vspace{-0.4cm}
\begin{block}{Why is the mixing time important?}
  \pause
  \begin{itemize}[<+->]
  \item If $T_t$ models thermal noise, $\tau_{\text{mix}}$ is the \alert<2>{lifetime} of information stored in the initial state $\rho$.

    The slower $\tau_{\text{mix}}$ scales with $\abs{\Lambda}$, the better!
    
  \item If $T_t$ models engineered dissipation preparing a desired state $\sigma$, then $\tau_{\text{mix}}$ is the \alert<3>{waiting time}.

  \item \alert<4>{Stability}: if $\tau_{\text{mix}} \sim \log \abs{\Lambda}$, then $T_t$ is \emph{stable} against local perturbations $L_u' = L_u + E_u$ (Cubitt, L., Michalakis, Perez-Garcia, CMP 2015).
  \end{itemize}
\end{block}
\end{frame}

\begin{frame}[standout]
  \center How to control the mixing time?
\end{frame}


\begin{frame}{Functional inequalities - I}
  \begin{block}{Dirichlet form}
    \[ \mathcal E(A,B) = \innerproduct{A}{-L^*(B)}_\sigma =
      \innerproduct{-L^*(A)}{B}_\sigma \]
  \end{block}
    \begin{block}{Variance}
    \[ Var_{\sigma}(A) =
      \norm{A-\innerproduct{A}{\identity}_\sigma}_{sigma}^2  =
      \norm{A}^2_{\sigma} - \tr(\sigma A)^2 \]
  \end{block}
    \begin{block}{Poincaré inequality}
    \[  c\, Var_{\sigma}(A) \le \mathcal E(A) \]
    The optimal constant $c$ is the \emph{spectral gap} of $L$: the smallest
    non-zero absolute value of eigenvalues of $L$.
  \end{block}

  \pause
  \begin{block}{Theorem}
    If $L$ has spectral gap $\lambda$, then $\tau_{\text{mix}} \sim \abs{\Lambda}/\lambda$
  \end{block}
\end{frame}


\begin{frame}{Functional inequalities - II}
  \begin{block}{Umegaki's Quantum relative entropy}
    \[\ent{\rho}{\sigma} = \Tr[\rho(\log\rho-\log\sigma)]\]
  \end{block}
  \begin{block}{Entropy production}
    \[ \mcl K(\rho) = - \Tr[L(\rho)(\log \rho - \log \sigma)]\] 
  \end{block}
  \pause
  \begin{block}{1-log-Sobolev inequality (or modified LSI)}
    \[ 2c\, \ent{\rho}{\sigma} \le \mcl K(\rho) \]
    the optimal constant $\alpha_1$ is the 1-log-Sobolev constant.
  \end{block}
\begin{block}{Theorem}
    If $L$ has 1-log-Sobolev constant $\alpha_1$, then $\tau_{\text{mix}} \sim \log(\abs{\Lambda})/\alpha_1$
  \end{block}
  \pause
  \alert{
    The log-Sobolev estimate guarantees stability!
  }
\end{frame}

\begin{frame}[standout]
  \center Summing up...
\end{frame}

\begin{frame}{Quantum semigroups and mixing time}
  \begin{itemize}[<+->]
    \item The mixing time controls important properties of quantum Markovian
      semigroups
    \item We can obtain estimates by showing various functional inequalities
    \item We can sometimes obtain very general results:
      \begin{theorem}[Bardet, Capel, Gao, L., Pérez-García, Rouzé. In
        preparation]
        Thermalization of 1D commuting spin chains satisfies a log-Sobolev inequality at
        any temperature.
      \end{theorem}
    \item Sometimes specific knowledge of the system is very important
      \begin{theorem}[{L., Pérez-García, Pérez-Hernández. arXiv:2107.01628}]
        Thermalization of a class of 2D quantum memories known as Kitaev's
        quantum double models have a non-zero spectral gap.
      \end{theorem}
    \end{itemize}
    \pause
    {
      \center
      \textbf{Thank you for your attention!}
      }
\end{frame}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
